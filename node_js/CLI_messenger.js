const prompt = require('async-prompt');
var select_chat_id = '';
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
const Messenger = require('./kaiju-messenger/kaiju-messenger-client-module.js');
const client = new Messenger();
async function messagelist(chat_id){
   let temp =  await client.get_db().prepare("select * from message where chat_id = ?").all(chat_id);
   console.log(temp);
}
async function chatlist(){
    let temp = await client.get_db().prepare("select * from chat").all()
    console.log(temp);
}
async function client_update_profile(data, data_type){
    let temp = JSON.parse(data);
    console.log(temp);
    await client.client.update_profile(temp,data_type);

}
async function client_search_user(username, offset, limit){
    let temp = await client.client.search_user(username, offset, limit);
    console.log(temp);

}
 function select_chat_id_method(params){
    select_chat_id = params;
}
async function client_info(){
    let temp = await client.client.info();
    console.log(temp);
}
const react_callback = {
    "message":{
        "send":function(data){
            console.log("New message");
            console.log(data["data"]);
        },
        "update":function(data){
            console.log("Update message");
            console.log(data["data"]);
        },
        "delete":function(data){
            console.log("Delete message");
            console.log(data["data"]);
        },
        "mark_as_read":function(data){
            console.log("Readed message");
            console.log(data["data"]);
        },
        "mark_as_arrived":function(data){
            console.log("Arrived message");
            console.log(data["data"]);
        }
    },
    "chat":{
        "create":function(data){
            console.log("New chat");
            console.log(data["data"]);
        },
        "leave":function(data){
            console.log("Leave chat");
            console.log(data["data"]);
        },
        "invite_user":function(data){
            console.log("Chat invite_user");
            console.log(data["data"]);
        },
        "delete":function(data){
            console.log("Chat delete");
            console.log(data["data"]);
        },
        "new_name":function(data){
            console.log("Chat new_name");
            console.log(data["data"]);
        },
        "settings":function(data){
            console.log("Chat settings");
            console.log(data["data"]);
        },
    }
};
async function main() {
    const login_or_register = await prompt('login or register:(login):');
    if(login_or_register == "" || login_or_register == "login"){
        const login = await prompt('Enter a login(test32):');
        const password = await prompt('Enter a password(test23test23test23):');
        if (login == ''){
            await client.client.login("test23", "test23test23test23");
        }
        else{
            await client.client.login(login, password);
        }
    }else{
        const login = await prompt('Enter a login:');
        const email = await prompt('Enter a email:');
        const password = await prompt('Enter a password:');
        await client.client.register(login, email, password);
        await client.client.login(login, password);
        
    }

    await client.init();
    for (i in react_callback){
        for(j in react_callback[i]){
            await client.add_callback(i,j,react_callback[i][j]);
        }
    }
    const commands = {
    "client_update_profile":{
        "params":{
            "data":"Enter data:",
            "data_type":"Enter data type(public private settings):"
        },
        "method":client_update_profile
    },
    "client_search_user":{
        "params":{
            "username":"Enter username:",
            "offset":"Enter offset:",
            "limit":"Enter limit:"
        },
        "method":client_search_user
    },
    "client_change_password":{
        "params":{
            "password":"Enter old password:",
            "new password":"Enter new password:"
        },
        "method":client.client.change_password.bind(client.cliet)
    },
    "new_data":{
        "params":{
        },
        "method":client.new_data.bind(client)
    },
    "client_info":{
        "params":{
        },
        "method": client_info

    },
    "chat_list":{
        "params":{
        },
        "method": chatlist

    },
    "message_list":{
        "params":{
            "chat_id":"Enter chat id:"
        },
        "method": messagelist

    },
    "chat_create":{
        "params":{
            "name":"Enter chat name:"
        },
        "method": client.chat.create.bind(client.chat)

    },
    "chat_invite":{
        "params":{
            "chat_id":"Enter chat id:",
            "username": "Enter username:"
        },
        "method": client.chat.invite.bind(client.chat)

    },
    "chat_leave":{
        "params":{
            "chat_id":"Enter chat id:"
        },
        "method": client.chat.leave.bind(client.chat)

    },
    "chat_new_name":{
        "params":{
            "chat_id":"Enter chat id:",
            "chat_name":"Enter chat name:"
        },
        "method": client.chat.new_name.bind(client.chat)

    },
    "chat_change_owner":{
        "params":{
            "chat_id": "Enter chat id:",
            "new_owner_id": "Enter new owner id:"

        },
        "method": client.chat.change_owner.bind(client.chat)

    },
    "chat_settings":{
        "params":{
            "chat_id":"Enter chat id:",
            "new_setting":"Enter new settings:"
        },
        "method": client.chat.settings.bind(client.chat)

    },
    "chat_delete":{
        "params":{
            "chat_id":"Enter chat id:"
        },
        "method": client.chat.delete.bind(client.chat)
    },
    "message_send":{
        "params":{
            "chat_id":"Enter chat id:",
            "data":"Enter data:"
        },
        "method": client.message.send.bind(client.message)
    },
    "message_delete":{
        "params":{
            "chat_id":"Enter chat id:",
            "message_id":"Enter message id:",
            "bucket":"Enter bucket:"
        },
        "method": client.message.delete.bind(client.message)
    },
    "message_update":{
        "params":{
            "chat_id":"Enter chat id:",
            "message_id":"Enter message id:",
            "bucket":"Enter bucket:",
            "new_data":"new_data"
        },
        "method": client.message.update.bind(client.message)
    },
    "message_mark_as_read":{
        "params":{
            "chat_id":"Enter chat id:",
            "message_id":"Enter message id:",
            "bucket":"Enter bucket:",
        },
        "method": client.message.mark_as_read.bind(client.message)
    },
    "message_mark_as_arrived":{
        "params":{
            "chat_id":"Enter chat id:",
            "message_id":"Enter message id:",
            "bucket":"Enter bucket:",
        },
        "method": client.message.mark_as_arrived.bind(client.message)
    },
    "select_chat":{
        "params":{
            "chat_id":"Enter chat id:"
        },
        "method":select_chat_id_method
    },
    "select_message":{
        "params":{
            "chat_id":"Enter chat id:"
        }
    }

};
    await sleep(600);
    await client.new_data();
    await sleep(600);
    let command = "";
    let result = ""
    while(true){
        await sleep(600);
        command = await prompt('Enter a command: ');
        if(command in commands){
            let temp = [];
            for (i in commands[command]['params']){
                if(i == "chat_id" && select_chat_id != ''){
                    result = await prompt(commands[command]['params'][i]+"("+select_chat_id+"):");
                    if (result==""){
                        result=select_chat_id;
                    }
                }else{
                    result = await prompt(commands[command]['params'][i]);
                }
                temp.push(result);
            }
            console.log(...temp);
            await commands[command]["method"](...temp);
        }else if(command == "help"){
            for (i in commands){
                console.log(i);
            }
        }else{
            console.log("Not this method");
        }
        // if(command == "chat_list"){
        //     console.log(chatlist());
        // }else if(command == "chat_delete"){
        //     let chat_id = prompt('Enter chat id: ');
        //     client.chat.delete(chat_id);
        // }else if(command == "chat_create"){
        //     let chat_name = prompt('Enter chat name: ');
        //     await client.chat.create(chat_name);
        // }else if(command == "chat_invite"){
        //     let chat_id = prompt('Enter chat id: ');
        //     let username = prompt('Enter username: ');
        //     await client.chat.invite(chat_id, username);
        // }else if(command == "chat_leave"){
        //     let chat_id = prompt('Enter chat id: ');
        //     await client.chat.leave(chat_id);
        // }else if(command == "chat_new_name"){
        //     let chat_id = prompt('Enter chat id: ');
        //     let new_name= prompt('Enter new name: ');
        //     await client.chat.new_name(chat_id, new_name);
        // }else if(command == "chat_change_owner"){
        //     let chat_id = prompt('Enter chat id: ');
        //     let new_owner_id = prompt('Enter  new owners id: ');
        //     await client.chat.change_owner(chat_id, new_owner_id);
        // }else if(command == "chat_list"){

        // }else if(command == "chat_list"){

        // }

    }
}

main().catch(err => {
    console.log(err)
})
