const https = require('http');
const default_request  = {
                  hostname: '0.0.0.0',
                  port: 8787,
                  path: '/public/rpc',
                  method: 'POST',
                  headers: {
                    'Content-Type': "text/plain",
                  }
                };
function httpsPost({body, ...options}) {
    return new Promise((resolve,reject) => {
        const req = https.request({
            method: 'POST',
            ...options,
        }, res => {
            const chunks = [];
            const cookie = res.headers['set-cookie'];
            res.on('data', data => chunks.push(data));
            res.on('end', () => {
                let resBody = Buffer.concat(chunks).toString()
                resolve([resBody,cookie])
            })
        })
        req.on('error',reject);
        if(body) {
            req.write(body);
        }
        req.end();
    })
}

class Client {
    constructor() {
        this.id = null;
        this.login_field = null;
        this.password = null;
        this.settings = null;
        this.refresh_token = null;
        this.access_token = null;  
    }
    async login(login, password){
        const options = {
                  ...default_request,
                  "body": JSON.stringify( {"method": "Clients.login", 
                                         "params": {
                                                      "username": login,
                                                      "password": password
                                                    }})
                };
        let temp = await httpsPost(options);
        temp = JSON.parse(temp[0]);
        if ('result' in temp){
            this.refresh_token = temp['result']['refresh'];
            this.access_token = temp['result']['access'];
            this.login_field = login;
            this.password = password;
        }else{
            throw new Error('Not correct data');
        }
        this.info();
    }
    async refresh(){
        let options = {
                      ...default_request,
                      "body": JSON.stringify({
                                            "method": "Clients.jwt_refresh", 
                                            "params": {
                                                        "refresh": this.refresh_token
                                                      }
                                                })
                    };
        let temp = await httpsPost(options);
        temp = JSON.parse(temp[0]);
        if ('result' in temp){
            this.refresh_token = temp['result']['refresh'];
            this.access_token = temp['result']['access'];
        }else{
            throw new Error('Not correct data');
        }
    }
    async change_password(password, new_password){
        let options = {
                  ...default_request,
                  "body": JSON.stringify( {"method": "Clients.change_password", 
                                         "params": {
                                                      "access_token": this.access_token,
                                                      "password": password,
                                                      "new_password": new_password
                                                    }})
                };
        let temp = await httpsPost(options);
        temp = JSON.parse(temp[0]);
        if ('error' in temp){
            throw new Error('Not correct data');
        }
    }
    async register(login, email, password){
        let options = {
                          ...default_request,
                          "body": JSON.stringify({
                                                "method": "Clients.register", 
                                                "params": {
                                                            "username": login,
                                                            "email": email,
                                                            "password": password
                                                           }
                                                       })
                        };
        let temp = await httpsPost(options);
        temp = JSON.parse(temp[0]);
        if ('result' in temp){
            this.refresh_token = temp['result']['refresh'];
            this.access_token = temp['result']['access'];
            this.login_field = login;
            this.password = password;
        }else{
            console.log(temp);
            throw new Error('Not correct data');
        }
    }
    async update_profile(data, data_type){
        let options = {
                              ...default_request,
                              "body": JSON.stringify({
                                                    "method": "Clients.update_profile", 
                                                    "params": {
                                                               "access_token": this.access_token,
                                                               "data": data,
                                                               "data_type": data_type
                                                              }
                                                          })
                            };

        let temp = await httpsPost(options);
        temp = JSON.parse(temp[0]);

        if ('error' in temp){
          console.log(temp['error'])
            throw new Error('Not correct data');
        }
    }
    async info(){
      if(this.access_token!=null){
          let options = {
                                ...default_request,
                                "body": JSON.stringify({
                                                      "method": "Clients.user_info", 
                                                      "params": {
                                                                  "access_token": this.access_token
                                                                }
                                                        })
                              };
          let temp = await httpsPost(options);
          temp = JSON.parse(temp[0]);
          if ('error' in temp){
              throw new Error('Not correct data');
          }else{
            this.id = temp['result']['id'];
          }
          return temp['result'];
        }
    }
    async search_user(username,offset,limit){
      if(this.access_token!=null){
          let body = JSON.stringify({
                                    "method": "Clients.search_clients", 
                                    "params": {
                                                "username": username,
                                                "offset": parseInt(offset, 10),
                                                "limit": parseInt(limit, 10),
                                                "access_token": this.access_token

                                              }
                                      });
          let options = {
                                ...default_request,
                                "body":body 
                              };
  
          let temp = await httpsPost(options);
          temp = JSON.parse(temp[0]);
          if ('error' in temp){
              throw new Error(JSON.stringify(temp['error']));
          }else{
            this.id = temp['result']['id'];
          }
          return temp['result'];
        }
    }
    

    logout(){
        this.id = null;
        this.username = null;
        this.password = null;
        this.settings = null;
        this.refresh_token = null;
        this.access_token = null;  
    }
}


module.exports = Client;