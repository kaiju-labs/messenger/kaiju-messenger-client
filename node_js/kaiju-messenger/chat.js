const { v1: uuidv1 } = require('uuid');
class Chat {
    add_callback(request_id,func){
        if (func != null){
            this.response_wait_dict[request_id] = func;
        }
    }
    check_callback(func){
        return function(data){
            func.bind(this)(data);
            if(this.response_wait_dict.hasOwnProperty(data['response_id'])){
                this.response_wait_dict[data['response_id']](data);
                delete this.response_wait_dict[data['response_id']];
            }
        }

    }
    constructor(db) {
        this.db = db;
        this.chat_table_create_query = 'CREATE TABLE IF NOT EXISTS chat (' +
                                       'chat_id TEXT UNIQUE, '+
                                       'settings TEXT , '+
                                       'owner_id TEXT , '+
                                       'created TEXT , '+ 
                                       'name UUID , '+
                                       'clients_list TEXT)';
        this.db.exec(this.chat_table_create_query);
        this.chat_insert = this.db.prepare('insert or replace into chat values (?,?,?,?,?,?)');
        this.chat_update_name_sql = this.db.prepare('update chat SET name = ? WHERE chat_id = ?');
        this.chat_update_owner_sql = this.db.prepare('update chat SET owner_id = ? WHERE chat_id = ?');
        this.chat_update_settings_sql = this.db.prepare('update chat SET settings = ? WHERE chat_id = ?');
        this.chat_update_clients_list_sql = this.db.prepare('update chat SET clients_list = ? WHERE chat_id = ?');
        this.chat_delete_sql = this.db.prepare('delete from chat where chat_id = ?');
        this.response_wait_dict = {}
        for (let key in this.react) {
            this.react[key] = this.check_callback(this.react[key]);
        }
    }
    init(ws){
        this.ws = ws;
    }
    list(f = null){
       let temp = {
            "method": "chat.list",
            "params": {
        }};
        temp["request_id"] = uuidv1();
        this.ws.send(JSON.stringify(temp));
        this.add_callback(temp["request_id"], f);
    }
    
    create(chat_name, f = null){
        let temp = {
            "method": "chat.create",
            "params": {
                "chat_name": chat_name
        }};
        temp["request_id"] = uuidv1();
        this.ws.send(JSON.stringify(temp));
        this.add_callback(temp["request_id"], f);
    }
    leave(chat_id, f = null){
        let temp = {
            "method": "chat.leave",
            "params": {
                "chat_id": chat_id
        }};
        temp["request_id"] = uuidv1();
        this.ws.send(JSON.stringify(temp));
        this.add_callback(temp["request_id"], f);
    }
    invite(chat_id, username, f = null){
        let temp = {
                "method": "chat.invite_user",
                "params": {
                    'chat_id': chat_id,
                    'username': username
            }};
        temp["request_id"] = uuidv1();
        this.ws.send(JSON.stringify(temp));
        this.add_callback(temp["request_id"], f);
    }
    delete(chat_id, f = null){
        let temp = {
                "method": "chat.delete",
                "params": {
                    'chat_id': chat_id
            }};
        temp["request_id"] = uuidv1();
        this.ws.send(JSON.stringify(temp));
        this.add_callback(temp["request_id"], f);
    }
    new_name(chat_id, new_name, f = null){
        let temp = {
                    'method': 'chat.new_name',
                    'params': {
                        'chat_id': chat_id,
                        'new_name': new_name
                    }};
        temp["request_id"] = uuidv1();
        this.ws.send(JSON.stringify(temp));
        this.add_callback(temp["request_id"], f);
    }
    change_owner(chat_id, new_owner_id, f = null){
        let temp = {
                    'method': 'chat.change_owner',
                    'params': {
                        'chat_id': chat_id,
                        'new_owner_id': new_owner_id
                    }};
        temp["request_id"] = uuidv1();
        this.ws.send(JSON.stringify(temp));
        this.add_callback(temp["request_id"], f);
    }
    settings(chat_id, settings, f = null){
        let temp = {
            'method': 'chat.settings',
            'params': {
                'chat_id': chat_id,
                'settings': settings
            }};
        temp["request_id"] = uuidv1();
        this.ws.send(JSON.stringify(temp));
        this.add_callback(temp["request_id"], f);
    }
    react_callback = {};
    react = {
        "chat.list": function(data){
          for (let value of data["data"]["chat_list"]) {
            this.chat_insert.run(
                value["chat_id"],
                value["settings"].toString(),
                value["owner_id"],
                value["created"],
                value["name"],
                JSON.stringify(value["clients_list"]));
            }
        },
        "chat.create":  function(data){
            this.list();
            console.log("chat_list")
        },                                                                   
        "chat.leave": function(data){
            if(data["status"]==='OK'){
                this.chat_update_clients_list_sql.run(JSON.stringify(data["data"]["clients_list"]), data["data"]["chat_id"]);
            }else{

            }
        },
        "chat.invite_user":function(data){
            if(data["status"]==='OK'){
                this.chat_update_clients_list_sql.run(JSON.stringify(data["data"]["clients_list"]), data["data"]["chat_id"]);
            }else{
                throw new Error(JSON.stringify({'method':'chat.invite','data': {'chat_id': data["data"]["chat_id"]}}));
            }
        },
        "chat.delete":function(data){
            if(data["status"]==='OK'){
                this.chat_delete_sql.run(data["data"]["chat_id"]);
            }else{
                throw new Error(JSON.stringify({'method':'chat.delete','status':data["status"],'data':{'chat_id':data["data"]["chat_id"]}}));
            }
        },
        "chat.new_name":function(data){
            if(data["status"]==='OK'){
                this.chat_update_name_sql.run(data["data"]["new_name"], data["data"]["chat_id"]);
            }else{

            }
        },
        "chat.settings":function(data){
            if(data["status"]==='OK'){
                this.chat_update_settings_sql.run(data["data"]["new_settings"], data["data"]["chat_id"]);
            }else{

            }
        },
        "chat.change_owner":function(data){
            if(data["status"]==='OK'){
                this.chat_update_owner_sql.run(data["data"]["new_owner_id"], data["data"]["chat_id"]);
            }else{

            }
        }

    }
}

module.exports = Chat;