const { v1: uuidv1 } = require('uuid');
class Message {
    // async wait_response(id){
    //     let count = 0;
    //     while (! (response_wait_array.includes(id.toString()))){
    //         await new Promise(resolve => setTimeout(resolve, 100));
    //         if (count > 99){
    //             return false;
    //         }else{
    //             count = count + 1;
    //         }
    //     }
    //     response_wait_array = response_wait_array.filter(function(value, index, arr){return value != id.toString();});
    //     console.log(response_wait_array);
    //     return true;
    // }
    add_callback(request_id,func){
        if (func != null){
            this.response_wait_dict[request_id] = func;
        }
    }
    check_callback(func){
        return function(data){
            func.bind(this)(data);
            if(this.response_wait_dict.hasOwnProperty(data['response_id'])){
                this.response_wait_dict[data['response_id']](data);
                delete this.response_wait_dict[data['response_id']];
            }
        }

    }

    constructor(db) {
        this.db = db;
        this.response_wait_dict = {};
        this.message_table_create_query = 'CREATE TABLE IF NOT EXISTS  message (' +
                                          'message_id TEXT UNIQUE, '+
                                          'bucket INT , '+
                                          'updated_time TEXT , '+
                                          'chat_id TEXT , '+
                                          'owner_id TEXT , '+
                                          'status INT ,'+
                                          'reader_status_list TEXT DEFAULT "[]" NOT NULL ,'+
                                          'data TEXT , '+
                                          'created_time TEXT , '+
                                          'type TEXT )';
        this.last_message_table_create_query ='CREATE TABLE IF NOT EXISTS  last_message (' +
                                              'message_id BLOB UNIQUE, '+
                                              'bucket INT , '+
                                              'chat_id TEXT )';
        this.db.exec(this.message_table_create_query);
        this.message_insert = this.db.prepare('insert or replace into message values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
        this.message_update_data_sql = this.db.prepare('update message SET data = ?, updated_time = ? WHERE message_id = ?');
        this.message_update_status_sql = this.db.prepare('update message SET status = ? WHERE message_id = ?');
        this.message_update_client_list_sql = this.db.prepare("update message SET" +
                                                              " reader_status_list = (select json_insert(message.reader_status_list,'$[#]',?) " +
                                                              " from message"+
                                                              " WHERE message_id = ?)"+
                                                              " WHERE message_id = ?");
        this.message_delete_sql = this.db.prepare('delete from message where message_id = ?'); 
        for (let key in this.react) {
             this.react[key] = this.check_callback(this.react[key]);
        }
    }
    init(ws){
        this.ws = ws;
    }

    send(chat_id, data, f = null){
        let temp = {
            'method': 'message.send',
            'params': {
                'chat_id': chat_id,
                'data': data
            }};
        temp["request_id"] = uuidv1();
        this.ws.send(JSON.stringify(temp));
        this.add_callback(temp["request_id"], f);
    }
    delete(chat_id, message_id, bucket, f = null){
        let temp = {
            'method': 'message.delete',
            'params': {
                'chat_id': chat_id,
                'message_info': {
                    "message_id":message_id,
                    "bucket":bucket
                }
            }};
        temp["request_id"] = uuidv1();
        this.ws.send(JSON.stringify(temp));
        this.add_callback(temp["request_id"], f);
    }
    update(chat_id, message_id, bucket, data, f = null){
       let temp = {
            'method': 'message.update',
            'params': {
                'chat_id': chat_id,
                'data': data,
                'message_info': {
                    "message_id":message_id,
                    "bucket":bucket,
                }
            }};
        temp["request_id"] = uuidv1();
        this.ws.send(JSON.stringify(temp));
        this.add_callback(temp["request_id"], f);
    }
    mark_as_read(chat_id, message_id, bucket, f = null){
    let temp = {
            'method': 'message.mark_as_read',
            'params': {
                'chat_id': chat_id,
                'message_info': {
                    "message_id":message_id,
                    "bucket":bucket
                }
            }};
        temp["request_id"] = uuidv1();
        this.ws.send(JSON.stringify(temp));
        this.add_callback(temp["request_id"], f);
    }
    mark_as_arrived(chat_id, message_id, bucket, f = null){
    let temp = {
            'method': 'message.mark_as_arrived',
            'params': {
                'chat_id': chat_id,
                'message_info': {
                    "message_id":message_id,
                    "bucket":bucket
                }
            }};
        temp["request_id"] = uuidv1();
        this.ws.send(JSON.stringify(temp));
        this.add_callback(temp["request_id"], f);
    }
    react_callback = {};
    react = {
        "message.list": function(data){
            for (let value of  data["data"]["message_list"]) {
            this.message_insert.run(
                value["message_info"]["message_id"],
                value["message_info"]["bucket"],
                "NULL",
                value["chat_id"],
                value["owner_id"],
                value["status"][0],
                JSON.stringify( value["status"][1]),
                value["data"],
                value["created_time"],
                value["type"]);

            }
        },
        "message.send": function(data){
            if(data["status"]==='OK'){
                let value = data["data"];
                console.log(typeof value["message_info"]["message_id"])
                this.message_insert.run(
                    value["message_info"]["message_id"],
                    value["message_info"]["bucket"],
                    "NULL",
                    value["chat_id"],
                    value["owner_id"],
                    value["status"][0],
                    JSON.stringify( value["status"][1]),
                    value["data"],
                    value["created_time"],
                    value["type"]);
            }
            else{
                throw new Error(JSON.stringify({'method':'message.send','data':{'message_id':data["data"]["message_id"]}}));
            }
        },
        "message.delete": function(data){
            if(data["status"]==='OK'){
                this.message_delete_sql.run(data["data"]["message_id"]);
            }
            else{
                throw new Error(JSON.stringify({'method':'message.delete','data':{'message_id':data["data"]["message_id"]}}));
            }
        },
        "message.update": function(data){
            if(data["status"]==='OK'){
                this.message_update_data_sql.run(data["data"]["data"],data["data"]["updated_time"], data["data"]["message_id"]);
            }
            else{
                throw new Error(JSON.stringify({'method':'message.update','data':{'message_id':data["data"]["message_id"]}}));
            }
        },
        "message.mark_as_read": function(data){
            if(data["status"]==='OK'){
                this.message_update_status_sql.run(2, data["data"]["message_id"]);
                this.message_update_client_list_sql.run(data["data"]["client_id"], data["data"]["message_id"], data["data"]["message_id"]);
            }else{
                throw new Error(JSON.stringify({'method':'message.mark_as_read','data':{'message_id':data["data"]["message_id"]}}));
            }
        },
        "message.mark_as_arrived": function(data){
            if(data["status"]==='OK'){
                this.message_update_status_sql.run(1, data["data"]["message_id"]);
            }else{
                throw new Error(JSON.stringify({'method':'message.mark_as_arrived','data':{'message_id':data["data"]["message_id"]}}));
            }
        }
    }
}
module.exports = Message;