const https = require('http');
const Database = require('better-sqlite3');
const { v1: uuidv1 } = require('uuid');
const WebSocket = require('ws');
const Chat = require('./chat.js');
const Message = require('./message.js');
const Client = require('./client.js');


class Messenger {
    constructor() {
        this.client = new Client();       
    }
    
    async init(){
        this.db = new Database(this.client.login_field+'.db', { verbose: console.log });
        this.chat = new Chat(this.db);
        this.message = new Message(this.db);
        if (this.client.access_token != null){
            this.ws = new WebSocket('ws://0.0.0.0:8780/ws');
            await this.connection();
            this.ws.on('message',this.on_message_dispacher.bind(this));
            this.ws.on('ping',this.pong.bind(this));
            this.ws.send(JSON.stringify({
                                    method:"login",
                                    params:{
                                            'access_token': this.client.access_token
                                        }
                                    }));
            this.chat.init(this.ws);
            this.message.init(this.ws);
        }else{
            throw new Error(JSON.stringify({'data':'Not login'}));
        }
    }
    pong(message){
        this.ws.pong(message);
    }
    get_db(){
        return this.db
    }
    add_callback(type, method, func){
        if(type =="chat"){
            this.chat.react_callback[type + '.' + method] = func;
        }       
        if(type =="message"){
            this.message.react_callback[type + '.' + method] = func;
        }
    }
    async close_ws(){
        let temp = {
                'method': 'close',
                'params': {}
        };
        temp["request_id"] = uuidv1();
        this.ws.send(JSON.stringify(temp));
        this.ws.close();
    }

    async connection (timeout = 10000) {
    let socket = this.ws;
      const isOpened = () => (socket.readyState === WebSocket.OPEN)
      if (socket.readyState !== WebSocket.CONNECTING) {
        return isOpened()
      }
      else {
        const intrasleep = 100
        const ttl = timeout / intrasleep
        let loop = 0
        while (socket.readyState === WebSocket.CONNECTING && loop < ttl) {
          await new Promise(resolve => setTimeout(resolve, intrasleep))
          loop++
        }
        return isOpened()
      }
    }

    on_message_dispacher(data){
        let data_temp = JSON.parse(data);
        
        if (typeof data_temp["data"] === 'object'){
            if (data_temp["method"] in this.chat.react){
                this.chat.react[data_temp["method"]].bind(this.chat)(data_temp);

            }
            else if (data_temp["method"] in this.message.react){
                this.message.react[data_temp["method"]].bind(this.message)(data_temp);
            }
            else{
                console.log(data_temp["method"]);
            }
            if (data_temp["method"] in this.chat.react_callback){
                this.chat.react_callback[data_temp["method"]].bind(this.chat)(data_temp);
            } else if (data_temp["method"] in this.message.react_callback){
                this.message.react_callback[data_temp["method"]].bind(this.message)(data_temp);
            }

        }
    }
    
    new_data(){
        this.db.prepare('delete from message').run();
        this.db.prepare('delete from chat').run();
        let temp = {
                'method': 'new_data',
                'params': {}
        };
        temp["request_id"] = uuidv1();
        this.ws.send(JSON.stringify(temp));
    }

}


module.exports = Messenger;